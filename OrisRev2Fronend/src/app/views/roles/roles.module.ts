import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';

import { RolesComponent } from './roles.component';
import { RolesRoutingModule } from './roles-routing.module';
//two way binding
import { FormsModule } from '@angular/forms'
// common popup modal
import { CommonModule } from '@angular/common';
//import { DialogModule } from '../dialog/dialog.module';
//datatable
import { DataTableModule } from 'primeng/primeng'; // Here
import {GrowlModule} from 'primeng/growl';
// import {Message} from 'primeng/components/common/api';
import {MessageService} from 'primeng/components/common/messageservice';
import {DialogModule} from 'primeng/dialog';

@NgModule({
  imports: [
    RolesRoutingModule,
    FormsModule,
    CommonModule,
    DataTableModule,
    DialogModule,
    GrowlModule
    // ChartsModule
  ],
  declarations: [ RolesComponent ],
  providers: [MessageService],
})
export class RolesModule { }
