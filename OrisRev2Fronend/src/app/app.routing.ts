import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { UserService} from "./views/user/user.service"

// Import Containers
import {
  FullLayoutComponent,
  SimpleLayoutComponent
} from './containers';



export const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },

  { path: 'login', loadChildren: './login/login.module#LoginModule' },
  
  { path: 'dashboard',component: FullLayoutComponent, loadChildren: './views/dashboard/dashboard.module#DashboardModule'  },
  { path: 'users',component: FullLayoutComponent,canActivate:[UserService], loadChildren: './views/user/user.module#UserModule' },
  { path: 'roles',component: FullLayoutComponent, loadChildren: './views/roles/roles.module#RolesModule' },
  { path: 'customers',component: FullLayoutComponent, loadChildren: './views/customer/customer.module#CustomerModule' },
   {
    path: 'pages',
    component: SimpleLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: './views/pages/pages.module#PagesModule',
      }
    ]
  },
  { path: "**",loadChildren: './login/login.module#LoginModule'}
   

];





@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}