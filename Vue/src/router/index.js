import Vue from 'vue'
import Router from 'vue-router'

// Containers
import Full from '@/containers/Full'
import Simple from '@/containers/Simple'
// Views
import Dashboard from '@/views/Dashboard'
import Login from '@/views/Login'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/dashboard',
      name: 'Home',
      component: Full,
      children: [
        {
          path: '/dashboard',
          name: 'Dashboard',
          component: Dashboard
        }

      ]
    },
    {
      path: '/',
      redirect: '/login',
      component: Simple,
      children: [
        {
          path: 'login',
          name: 'Login',
          component: Login
        }

      ]

    }
  ]
})
