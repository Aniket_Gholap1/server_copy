import { Component, OnInit } from '@angular/core';
import { RouterModule, Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss']
})
export class RolesComponent implements OnInit {
    public showDialog:boolean = false;//modal status
    public deleteDialog:boolean =false;
    public msgs:any=[];
    public modal_title:string = "Add Role";

    public transactions: {
      action:any ,
      name: string,
      description:string,
      read:boolean,
      write:boolean,
      delete:boolean,
    }[];
    public permissions:any =[];

    role:any = {};

  constructor(private router: Router) {
   }

  ngOnInit() {

    this.permissions =[
      { id :1, name:'User Management', all:false, read:false, write:false, delete:false },
      { id :2, name:'Customer Management', all:false, read:false, write:false, delete:false },
      { id :3, name:'Role Management', all:false, read:false, write:false, delete:false },
      { id :4, name:'Team Management', all:false, read:false, write:false, delete:false },
      { id :5, name:'Job Management', all:false, read:false, write:false, delete:false },
      
    ]

      this.transactions = [
                {action: '', name: 'Administartor', description: '', read:false, write:false, delete:false },
                {action: '', name: 'Summarizer',    description: '', read:false, write:false, delete:false },
                {action: '', name: 'Sorter',        description: '', read:false, write:false, delete:false },
                {action: '', name: 'Sorter',        description: '', read:false, write:false, delete:false },
        
      ]
  }

  all(position,list:any){
    if(this.permissions[position]['id']==list['id']){
      this.permissions[position]['all'] = !this.permissions[position]['all']
      this.permissions[position]['read'] = this.permissions[position]['all']
      this.permissions[position]['write'] = this.permissions[position]['all']
      this.permissions[position]['delete'] = this.permissions[position]['all']
    }else{
      //filter content
    }    
  }
  read(position,list:any){
    if(this.permissions[position]['id']==list['id']){
      this.permissions[position]['all'] = false;
      this.permissions[position]['read'] = !this.permissions[position]['read']
    }else{
      //filter content
    }    
  }
  write(position,list:any){
    if(this.permissions[position]['id']==list['id']){
      this.permissions[position]['all'] = false;
      this.permissions[position]['write'] = !this.permissions[position]['write']
    }else{
      //filter content
    }    
  }

  delete(position,list:any){
    if(this.permissions[position]['id']==list['id']){
      this.permissions[position]['all'] = false;
      this.permissions[position]['delete'] = !this.permissions[position]['delete']
    }else{
      //filter content
    }   
  }

  addRole(){
    this.role = '';
    this.showDialog = !this.showDialog;
    this.modal_title = "Add Role";
  }

  saveRolePermission(){
    console.log(this.role,'-----------role-----------------');
    console.log(this.permissions,'----------all-------permissions--------') 
  }

  deleteRole(){
    this.deleteDialog = true;//modal status
    this.modal_title = "Delete Role"
  }

  deleteUserRole(){
     this.showSuccess('Record has been deleted');
     this.deleteDialog =false;
  }
  showSuccess(recordStatus) {
        this.msgs =[{severity:'success', summary:recordStatus}];
  }

  editRole(data,position){
    this.modal_title = "Edit Role";
    console.log(data,'-------------data-----------')
    this.role = data;
    this.showDialog = true;    
  }



}
