// Initializes the `Message` service on path `/message`
const createService = require('feathers-mongodb');
const hooks = require('./message.hooks');
const filters = require('./message.filters');
var express = require('express')
var app = express()
const bodyParser = require("body-parser");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
var qs = require('querystring');
app.use(bodyParser.json());

module.exports = function () {
  const app = this;
  const paginate = app.get('paginate');
  const mongoClient = app.get('mongoClient');
  const options = { paginate };
  var MeassgeCollection;
  
  // Initialize our service with any options it requires
  app.use('/message', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('message');

  mongoClient.then(db => {
    service.Model = db.collection('message');
    console.log("connected to mongo db")
    MeassgeCollection = db.collection('message');
  
  });

app.route('/test')
  .get(function (req, res) {
    res.send('Get a userslist')
  })
  .post(function (req, res) {
    console.log(req.body)
    res.send('Add a user')
  })
  .put(function (req, res) {
    res.send('Update the user')
  })
  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
