const message = require('./message/message.service.js');
const test = require('./message/test.service.js');
const express = require('express')
module.exports = function () {
  const app = this; // eslint-disable-line no-unused-vars
  app.configure(message);
  app.configure(test);
};
